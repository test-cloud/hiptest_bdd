package com.testcloud.seleniumcourse.tests.login;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.gherkin.model.Scenario;
import com.testcloud.seleniumcourse.tests.Actionwords;
import com.testcloud.seleniumcourse.dataprovider.datasets.DataSet;
import com.testcloud.seleniumcourse.dataprovider.details.TestHelper;
import org.testng.annotations.Test;


import com.testcloud.seleniumcourse.config.TestManager;

public class UnsuccessfulUserAuthenticationTest extends TestManager{
    


    private static ExtentTest parentTest;
    private static Actionwords actionwords;

    @Test(testName="Unsuccessful user authentication",groups={"all",""},enabled = false,
    dataProvider = "unsuccessfulUserAuthentication",dataProviderClass = DataSet.class)
    public void unsuccessfulUserAuthentication(TestHelper testHelper)  throws Exception {

        parentTest = setParentTest(parentTest,".login");
        ExtentTest child = parentTest.createNode(Scenario.class, "Unsuccessful user authentication" + " " + testHelper.getDataSetNumber());
        test.set(child);
        actionwords = new Actionwords(driver.get(),test.get());

        // Given the user is on the login page
        actionwords.theUserIsOnTheLoginPage("Given the user is on the login page",testHelper);

        // When the user inputs the username
        actionwords.theUserInputsTheUsername("When the user inputs the username",testHelper);

        // And the user inputs the password
        actionwords.theUserInputsThePassword("And the user inputs the password",testHelper);

        // And the user clicks the Login button
        actionwords.theUserClicksTheLoginButton("And the user clicks the Login button",testHelper);

        // Then the user should be view a unSuccess message
        actionwords.theUserShouldBeViewAUnSuccessMessage("Then the user should be view a unSuccess message",testHelper);
    }

}