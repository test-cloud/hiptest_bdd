package com.testcloud.seleniumcourse.tests;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.GherkinKeyword;
import com.testcloud.seleniumcourse.dataprovider.details.TestHelper;
import com.testcloud.seleniumcourse.misc.Gherkin;
import com.testcloud.seleniumcourse.pages.home.HomePage;
import com.testcloud.seleniumcourse.pages.login.LoginPage;
import org.openqa.selenium.WebDriver;
import com.testcloud.seleniumcourse.config.TestManager;

public class Actionwords extends TestManager {
    protected WebDriver driver;
    protected InheritableThreadLocal<LoginPage> loginPage = new InheritableThreadLocal<>();
    protected InheritableThreadLocal<HomePage> homePage = new InheritableThreadLocal<>();
    protected ExtentTest test;

    public Actionwords(WebDriver driver, ExtentTest test) {
        this.driver = driver;
        this.test = test;

    }

    public void theUserIsOnTheLoginPage(String s, TestHelper testHelper) throws Exception {
        Gherkin gherkin = new Gherkin(s);
        loginPage.set(new LoginPage(driver));
        try {
            loginPage.get().checkTheUserIsOnTheLoginPage();
            test.createNode(new GherkinKeyword(gherkin.getKeyword()), gherkin.getMessage()).pass("");
        } catch (Exception e) {
            test.createNode(new GherkinKeyword(gherkin.getKeyword()), gherkin.getMessage()).fail("");
            throw e;

        }
    }

    public void theUserInputsTheUsername(String s, TestHelper testHelper) throws Exception {
        Gherkin gherkin = new Gherkin(s);
        try {
            loginPage.get().typeInUsernameField(testHelper.getLogin().getUsername());
            test.createNode(new GherkinKeyword(gherkin.getKeyword()), gherkin.getMessage()).pass("");
        } catch (Exception e) {
            test.createNode(new GherkinKeyword(gherkin.getKeyword()), gherkin.getMessage()).fail("");
            throw e;
        }

    }

    public void theUserInputsThePassword(String s, TestHelper testHelper) throws Exception {
        Gherkin gherkin = new Gherkin(s);
        try {
            loginPage.get().typeInPassWordField(testHelper.getLogin().getPassword());
            test.createNode(new GherkinKeyword(gherkin.getKeyword()), gherkin.getMessage()).pass("");
        } catch (Exception e) {
            test.createNode(new GherkinKeyword(gherkin.getKeyword()), gherkin.getMessage()).fail("");
            throw e;
        }

    }

    public void theUserClicksTheLoginButton(String s, TestHelper testHelper) throws Exception {
        Gherkin gherkin = new Gherkin(s);
        try {
            loginPage.get().clickOnSubmitButton();
            test.createNode(new GherkinKeyword(gherkin.getKeyword()), gherkin.getMessage()).pass("");
        } catch (Exception e) {
            test.createNode(new GherkinKeyword(gherkin.getKeyword()), gherkin.getMessage()).fail("");
            throw e;
        }
    }


    public void theUserShouldBeRedirectedToHomePage(String s, TestHelper testHelper) throws Exception {
        Gherkin gherkin = new Gherkin(s);
        homePage.set(new HomePage(driver));
        try {
            homePage.get().checkTheUserIsInTheHomePage();
            test.createNode(new GherkinKeyword(gherkin.getKeyword()), gherkin.getMessage()).pass("");
        } catch (Exception e) {
            test.createNode(new GherkinKeyword(gherkin.getKeyword()), gherkin.getMessage()).fail("");
            throw e;

        }

    }

    public void theUserShouldBePresentedWithASuccessMessage(String s, TestHelper testHelper) throws Exception {
        Gherkin gherkin = new Gherkin(s);
        try {
            test.createNode(new GherkinKeyword(gherkin.getKeyword()), gherkin.getMessage()).pass("");
        } catch (Exception e) {
            test.createNode(new GherkinKeyword(gherkin.getKeyword()), gherkin.getMessage()).fail("");
            throw e;

        }

    }

    public void theUserShouldBeViewAUnSuccessMessage(String s, TestHelper testHelper) throws Exception {
        Gherkin gherkin = new Gherkin(s);
        try {
            loginPage.get().checkWarnMessage();
            test.createNode(new GherkinKeyword(gherkin.getKeyword()), gherkin.getMessage()).pass("");
        } catch (Exception e) {
            test.createNode(new GherkinKeyword(gherkin.getKeyword()), gherkin.getMessage()).fail("");
            throw e;

        }
    }
}