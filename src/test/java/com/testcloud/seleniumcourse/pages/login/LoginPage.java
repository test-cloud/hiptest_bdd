package com.testcloud.seleniumcourse.pages.login;

import org.openqa.selenium.WebDriver;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.StringContains.containsString;

public class LoginPage {

    private WebDriver driver;
    private Factory elements;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        this.elements = new Factory(driver);
    }

    public void typeInUsernameField(String username) {
        assertThat("Username field is not displayed", elements.username.isDisplayed(), equalTo(true));
        elements.username.clear();
        elements.username.sendKeys(username);
    }

    public void typeInPassWordField(String password) {
        assertThat("Password field is not displayed", elements.password.isDisplayed(), equalTo(true));
        elements.password.clear();
        elements.password.sendKeys(password);
    }

    public void clickOnSubmitButton() {
        assertThat("Submit button is not displayed", elements.submitButton.isDisplayed(), equalTo(true));
        assertThat("Submit button is not enabled", elements.submitButton.isEnabled(), equalTo(true));
        elements.submitButton.click();

    }

    public void checkTheUserIsOnTheLoginPage() {
        assertThat("The user is not in \"Login page\"", driver.getCurrentUrl(), containsString("login"));
    }

    public void checkWarnMessage() throws Exception {
        assertThat("Warn message is not displayed", elements.warnMessage.isDisplayed(), equalTo(true));
        assertThat("Warn message is not equal to \"credentials are incorrect :-/\"", elements.warnMessage.getText(), equalTo("credentials are incorrect :-/"));
    }

    public void checkTheUserIsAuthenticated() {

    }

    public void checkTheUserIsInTheHomePage() {

    }
}
