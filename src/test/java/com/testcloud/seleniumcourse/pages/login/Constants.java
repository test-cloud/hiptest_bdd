package com.testcloud.seleniumcourse.pages.login;

public class Constants {

    static final String USERNAME_ID = "username";
    static final String PASSWORD_ID = "password";
    static final String SUBMIT_BUTTON_ID = "login";
    static final String WARN_MESSAGE_ID = "warn-message";

}
