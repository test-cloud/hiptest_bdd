package com.testcloud.seleniumcourse.pages.login;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Factory extends Constants {

    @FindBy(id = USERNAME_ID)
    WebElement username;
    @FindBy(id = PASSWORD_ID)
    WebElement password;
    @FindBy(id = SUBMIT_BUTTON_ID)
    WebElement submitButton;
    @FindBy(id = WARN_MESSAGE_ID)
    WebElement warnMessage;

    public Factory(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}
