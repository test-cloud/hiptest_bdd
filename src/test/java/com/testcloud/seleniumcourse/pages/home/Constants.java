package com.testcloud.seleniumcourse.pages.home;

public class Constants {
    static final String WELCOME_PANEL_ID = "welcome-panel";
    static final String HOME_TAB_XPATH = "//ul[@class='nav navbar-nav']/li[1]";
    static final String LESSONS_TAB_XPATH = "//ul[@class='nav navbar-nav']/li[2]";
    static final String ABOUT_US_TAB_XPATH = "//ul[@class='nav navbar-nav']/li[3]";
    static final String LOGOUT_TAB_XPATH = "//ul[@class='nav navbar-nav']/li[4]";
}
