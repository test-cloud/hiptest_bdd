package com.testcloud.seleniumcourse.pages.home;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

public class HomePage {

    private WebDriver driver;
    private Factory elements;
    private WebDriverWait wait;

    public HomePage(WebDriver driver) {
        this.driver = driver;
        this.elements = new Factory(this.driver);
        this.wait = new WebDriverWait(this.driver,10);
    }

    public void checkTheUserIsInTheHomePage() {
        wait.until(ExpectedConditions.visibilityOfAllElements(Arrays.asList(elements.homeTab,elements.lessonsTab,elements.aboutUsTab,elements.logoutTab)));
    }

    public void checkSuccessMessage() {
        assertThat("Welcome message is not displayed", elements.welcomePanel.isDisplayed(), equalTo(true));
        assertThat("Warn message is not equal to \"Welcome, Matt! ,)\"", elements.welcomePanel.getText(), equalTo("Welcome, Matt! ,)"));


    }
}
