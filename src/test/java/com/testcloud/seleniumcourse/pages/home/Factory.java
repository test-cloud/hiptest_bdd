package com.testcloud.seleniumcourse.pages.home;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class Factory extends Constants {

    @FindBy(id = WELCOME_PANEL_ID)
    WebElement welcomePanel;
    @FindBy(xpath = HOME_TAB_XPATH)
    WebElement homeTab;
    @FindBy(xpath = LESSONS_TAB_XPATH)
    WebElement lessonsTab;
    @FindBy(xpath = ABOUT_US_TAB_XPATH)
    WebElement aboutUsTab;
    @FindBy(xpath = LOGOUT_TAB_XPATH)
    WebElement logoutTab;


    public Factory(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}
