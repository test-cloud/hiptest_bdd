package com.testcloud.seleniumcourse.dataprovider.details;

public class TestHelper {

    private String dataSetNumber;
    private Login login;

    public TestHelper(String dataSetNumber,Login login) {
        this.dataSetNumber = dataSetNumber;
        this.login = login;
    }

    public String getDataSetNumber() {
        return dataSetNumber;
    }

    public Login getLogin() {
        return login;
    }
}
