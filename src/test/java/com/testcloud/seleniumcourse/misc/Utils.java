package com.testcloud.seleniumcourse.misc;

import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.MediaEntityModelProvider;
import com.testcloud.seleniumcourse.config.environment.ConfigRunner;
import com.testcloud.seleniumcourse.config.environment.EvidenceType;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.util.Date;

/**
 * Created by MiguelDelgado on 5/29/17.
 */
public class Utils {

    public static synchronized MediaEntityModelProvider takeScreenshot(WebDriver driver, EvidenceType evidence) throws Exception {
        File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        File out;
        String evidenceName = String.valueOf(new Date().getTime()) + ".png";
        FTP ftp;
        MediaEntityModelProvider mediaEntityBuilder;

        if (evidence.type) {
            out = new File(ConfigRunner.local_success_evidence_path + evidenceName);
            FileUtils.copyFile(src, out);
            src.delete();
            ftp = new FTP(ConfigRunner.remote_host, ConfigRunner.ftp_user, ConfigRunner.ftp_password, ConfigRunner.success_evidence_path);
            ftp.uploadFile(out.getAbsolutePath(), evidenceName);
            mediaEntityBuilder = MediaEntityBuilder.createScreenCaptureFromPath(ConfigRunner.remote_success_evidence_path + evidenceName).build();
            out.delete();
        } else {
            out = new File(ConfigRunner.local_error_evidence_path + evidenceName);
            FileUtils.copyFile(src, out);
            src.delete();
            ftp = new FTP(ConfigRunner.remote_host, ConfigRunner.ftp_user, ConfigRunner.ftp_password, ConfigRunner.error_evidence_path);
            ftp.uploadFile(out.getAbsolutePath(), evidenceName);
            mediaEntityBuilder = MediaEntityBuilder.createScreenCaptureFromPath(ConfigRunner.remote_error_evidence_path + evidenceName).build();
            out.delete();
        }
        return mediaEntityBuilder;
    }
}
