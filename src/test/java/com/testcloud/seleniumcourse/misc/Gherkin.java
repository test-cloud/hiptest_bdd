package com.testcloud.seleniumcourse.misc;

public class Gherkin {
    private String keyword;
    private String message;

    public Gherkin(String s) {
        this.keyword = initKeyword(s);
        this.message = initMessage(s);
    }

    private String initMessage(String message) {
        return message.split(" ", 2)[1];
    }

    private String initKeyword(String message) {
        return message.split(" ", 2)[0];
    }

    public String getKeyword() {
        return this.keyword;
    }

    public String getMessage() {
        return this.message;
    }
}
