package com.testcloud.seleniumcourse.config;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.testcloud.seleniumcourse.config.environment.ConfigRunner;
import com.testcloud.seleniumcourse.config.report.ExtentManager;
import org.apache.commons.lang3.text.WordUtils;
import org.openqa.selenium.Platform;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by MiguelDelgado on 6/2/17.
 */
public class TestManager {

    private static ExtentReports extent;
    private static Collection<ExtentTest> parentTestList;
    protected static InheritableThreadLocal<ExtentTest> test = new InheritableThreadLocal<>();
    public static InheritableThreadLocal<RemoteWebDriver> driver = new InheritableThreadLocal<>();

    @BeforeSuite(alwaysRun = true)
    public void beforeSuite() throws Exception {
        parentTestList = new ArrayList<>();
        extent = ExtentManager.createInstance("target/extent.html");
        new ConfigRunner();
    }

    @BeforeMethod(alwaysRun = true)
    public void setUp() throws Exception {
        try {
            driver.set(new RemoteWebDriver(new URL(ConfigRunner.seleniumGrid), new DesiredCapabilities(ConfigRunner.browser, "", Platform.ANY)));
            driver.get().get(ConfigRunner.environment);
            driver.get().manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        } catch (Exception e) {
            throw new RuntimeException("driver not load");
        }
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        if (result.getStatus() == ITestResult.FAILURE) {
            test.get().fail(result.getThrowable());
        } else if (result.getStatus() == ITestResult.SKIP)
            test.get().skip(result.getThrowable());
        else
            test.get().pass("Test passed");
        extent.flush();
        driver.get().quit();
    }


    protected ExtentTest setParentTest(ExtentTest parentTest, String name) {
        synchronized (parentTestList) {
            name = WordUtils.capitalize(name.replace(".", " ").trim());
            if (parentTestList.isEmpty()) {
                parentTest = extent.createTest(name);
                parentTest.getModel().setStartTime(new Date());
                parentTestList.add(parentTest);
            } else {
                boolean notFound = true;
                for (ExtentTest pTest : parentTestList) {
                    if (pTest.getModel().getName().equals(name)) {
                        parentTest = pTest;
                        notFound = false;
                        break;
                    }
                }
                if (notFound) {
                    parentTest = extent.createTest(name);
                    parentTest.getModel().setStartTime(new Date());
                    parentTestList.add(parentTest);
                }
            }
            return parentTest;
        }
    }


}
