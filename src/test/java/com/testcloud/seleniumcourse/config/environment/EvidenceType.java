package com.testcloud.seleniumcourse.config.environment;

public enum EvidenceType {
    SUCCESS(true),
    ERROR(false);
    public boolean type;

    EvidenceType(Boolean type) {
        this.type = type;
    }
}
