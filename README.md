hiptest-publisher --config=src/test/resources/hiptest/hiptest-publisher.conf --show-actionwords-diff

hiptest-publisher --config=src/test/resources/hiptest-publisher.conf --show-actionwords-deleted

hiptest-publisher --config=src/test/resources/hiptest-publisher.conf --show-actionwords-created

hiptest-publisher --config=src/test/resources/hiptest-publisher.conf --show-actionwords-renamed

hiptest-publisher --config=src/test/resources/hiptest-publisher.conf --show-actionwords-signature-changed


_Command to import only test from HipTest:_

    hiptest-publisher --config=src/test/resources/hiptest/hiptest-publisher.conf --only=tests
    
    Note: Please note that all test imported with this command are disabled, 
    so if you want run an specific test delete this parameter in annotation @Test
    
_Command to import only test from HipTest associated an specific test run:_

    hiptest-publisher --config=src/test/resources/hiptest/hiptest-publisher.conf --only=tests --test-run-name=TEST_RUN_NAME
    
_Command to update actionwords_signature.yaml file (this has to be done when you add new actionWord or you modify one):_

    hiptest-publisher --config=src/test/resources/hiptest/hiptest-publisher.conf --actionwords-signature
     
_Command to see action words created:_

    hiptest-publisher --config=src/test/resources/hiptest/hiptest-publisher.conf --show-actionwords-created
    
_Command to see which action word signature was changed:_

    hiptest-publisher --config=src/test/resources/hiptest/hiptest-publisher.conf --show-actionwords-signature-changed
